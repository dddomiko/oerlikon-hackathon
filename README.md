# oerlikon - Hackathon

## Website

https://www.oerlikon.com/en/oerlikon-digital-hub/hackathon/

## Project Duration

08.11.2019 - 10.11.2019

## Project Team

* Dominik Koch
* Martin Hillebrand
* Giacomo Snidero
* Nurbol Sakenov

## Short Description

tbd

## Goals:

tbd

## Tools

* R
* Python
* Microsoft Azure

## Repo Structure

```bash

.
└── oerlikon-hackathon
    ├── README.md
    ├── code
    │   ├── R
    │   ├── RShiny
    │   └── python
    ├── data
    │   ├── cleaned
    │   └── raw
    ├── documents
    │   ├── description
    │   ├── literature
    │   └── presentation
    └── output
        ├── figures
        └── tables

```
