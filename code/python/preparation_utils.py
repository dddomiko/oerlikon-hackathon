import datetime
import pandas as pd


BAD_SENSOR = [9070, 9071, 9072, 9191, 9192]


#take long format sensor data, removes unnecessary columns, 
# corrects timestamp, brings it in wide format
def raw_long_to_wide(df):
    # correct timestamp
    df["Timestamp"] = pd.to_datetime(df.Timestamp.str.slice(start=0, stop=19, step=None))
    df = df.drop_duplicates()
    df = df[~df.ValueID.isin(BAD_SENSOR)]
    df_wide = df.pivot("Timestamp", "ValueID", "RealValue")
    return df_wide


def get_slice_before_fault(df_wide,fault_ts):
    fault_before10 = fault_ts +datetime.timedelta(minutes=-10)
    fault_before5 = fault_ts +datetime.timedelta(minutes=-5)
    return(df_wide.loc[fault_before10:fault_before5])


def drop_constant_column(df):
    """
    Drops constant value columns of pandas dataframe.
    """
    return df.loc[:, (df != df.iloc[0]).any()]

